import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import pandas as pd
from datetime import datetime

df = pd.read_csv('tweets-exp-p2-1500.csv')
df['user_created_date'] = df['user_created_date'].apply(lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp())
df['posted_at'] = df['posted_at'].apply(lambda x: datetime.strptime(x, '%a %b %d %H:%M:%S %z %Y').timestamp())

df['posted_at'] = pd.to_datetime(df['posted_at']).dt.tz_localize('UTC')
current_time_utc = pd.Timestamp.now(tz='UTC')
df['no_days_posted'] = (current_time_utc - df['posted_at']).dt.days


df['user_created_date'] = pd.to_datetime(df['user_created_date']).dt.tz_localize('UTC')
current_time_utc = pd.Timestamp.now(tz='UTC')
df['no_days_user_created'] = (current_time_utc - df['user_created_date']).dt.days


df = df.drop(columns=['author_id', 'user_created_date', 'posted_at'])
print(df.columns)
