import matplotlib.pyplot as plt
import matplotlib

x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]
plt.plot(x, y)
plt.xlabel('X-axis')
plt.ylabel('Y-axis')
plt.title('Sample Plot')
plt.savefig('sample.jpg', format='jpeg')
plt.clf()