import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
import warnings
warnings.filterwarnings("ignore")

df = pd.read_csv('tweets-exp-p1-1500.csv')
#print(df.head())
#print(df.columns)

from datetime import datetime
#df['user_created_date'] = pd.to_datetime(df['user_created_date'])
df['user_created_date'] = df['user_created_date'].apply(lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp())
df = df.drop(columns=['text', 'author'])



y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

#print("Training set size: ", X_train.shape[0])
#print("Testing set size: ", X_test.shape[0])

#print(f'Data Split done.')
# Create a Decision Tree Classifier instance
#clf = DecisionTreeClassifier()
#clf = GaussianNB()
#clf = LogisticRegression()
#clf = KNeighborsClassifier(n_neighbors=5)
#clf = RandomForestClassifier(n_estimators=100, random_state=42)
#clf= SVC(kernel='rbf', C=1.0, gamma='scale')


classifiers = [
    DecisionTreeClassifier(),
    GaussianNB(),
    LogisticRegression(),
    KNeighborsClassifier(n_neighbors=5),
    RandomForestClassifier(n_estimators=100, random_state=42)
]


for clf in classifiers:
    clf_name = clf.__class__.__name__
    
    # Fit the classifier on the training data
    clf.fit(X_train, y_train)
    
    # Make predictions on the test data
    y_pred = clf.predict(X_test)
    
    # Calculate accuracy
    accuracy = accuracy_score(y_test, y_pred)
    
    # Print accuracy and other performance details
    print(f"Classifier: {clf_name}")
    print(f"Accuracy: {accuracy:.4f}")
    
    # Print confusion matrix
    cm = confusion_matrix(y_test, y_pred)
    #print("Confusion Matrix:")
    #print(cm)
    
    print("-" * 30)



