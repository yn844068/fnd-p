import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import numpy as np 

# Read the CSV file into a pandas DataFrame
df = pd.read_csv('old/net100k.csv')

# Create an empty DiGraph
G = nx.DiGraph()

# Add edges to the graph
for _, row in df.iterrows():
    follower = row[0]
    target = row[1]
    G.add_edge(follower, target)

#layout = nx.spring_layout(G)
#alpha is opaqueness 0(fully transparent) and 1 (fully opaque)
#nx.draw(G,pos=layout,alpha=0.2)
#plt.savefig('graph.jpg', format='jpeg')


print('Number of edges: ', G.number_of_edges() )
print('Number of nodes: ', G.number_of_nodes() )

avg_degree = np.mean([d for _, d in G.degree()])
print(' On average, a node is connected to ',avg_degree)



degree_centrality = nx.centrality.degree_centrality(G)
keys = list(degree_centrality.keys())
values = list(degree_centrality.values())
# Sort the values in descending order
values, keys = zip(*sorted(zip(values, keys), reverse=True))
# Select the top 10 values
top_values = values[:10]
top_keys = keys[:10]
x_pos = range(len(top_keys))
plt.bar(x_pos, top_values, align='center')
plt.xticks(x_pos, top_keys)
plt.xticks(rotation=45)
plt.xlabel('Nodes')
plt.ylabel('Degree')
plt.title('Top 10 nodes with highest degree')
plt.savefig('degree_centrality.jpg', format='jpeg')
#print('keys :', keys)
author_ids_to_remove = keys


betweenness_centrality = nx.centrality.betweenness_centrality(G)
keys = list(betweenness_centrality.keys())
values = list(betweenness_centrality.values())
# Sort the values in descending order
values, keys = zip(*sorted(zip(values, keys), reverse=True))
top_values = values[:10]
top_keys = keys[:10]
x_pos = range(len(top_keys))

plt.bar(x_pos, top_values, align='center')

plt.xticks(x_pos, top_keys)
plt.xticks(rotation=45)
plt.xlabel('Nodes')
plt.ylabel('Betweenness Centrality')
plt.title('Top 10 nodes with highest betweenness centrality')
plt.savefig('betweenness centrality.jpg', format='jpeg')




##############    preprocess and remove the 
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score,  confusion_matrix
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.svm import SVC
import warnings
warnings.filterwarnings("ignore")
df = pd.read_csv('tweets-exp-p2-15000.csv')
print(list(df.columns))
print("Number of rows in df:", len(df))

selected_rows = df[df['author_id'].isin(author_ids_to_remove)]
# Print the selected rows
print(selected_rows)


df = df[~(df['author_id'].isin(author_ids_to_remove) & (df['flag_real'] == False))]

print("After filtering, number of rows in df:", len(df))

df['user_created_date'] = df['user_created_date'].apply(lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp())
df['posted_at'] = df['posted_at'].apply(lambda x: datetime.strptime(x, '%a %b %d %H:%M:%S %z %Y').timestamp())
df['posted_at'] = pd.to_datetime(df['posted_at']).dt.tz_localize('UTC')
current_time_utc = pd.Timestamp.now(tz='UTC')
df['no_days_posted'] = (current_time_utc - df['posted_at']).dt.days
df['user_created_date'] = pd.to_datetime(df['user_created_date']).dt.tz_localize('UTC')
current_time_utc = pd.Timestamp.now(tz='UTC')
df['no_days_user_created'] = (current_time_utc - df['user_created_date']).dt.days
df = df.drop(columns=['author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type'])

#df = df.drop(columns=['retweet_count', 'reply_count', 'like_count', 'tweet_count',  'listed_count',  'quote_count', 'impression_count', 'followers_count', 'following_count'])
y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

clf=RandomForestClassifier(n_estimators=100, random_state=42)
clf_name = clf.__class__.__name__
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print(f"Classifier: {clf_name}")
print(f"Accuracy: {accuracy:.4f}")
#cm = confusion_matrix(y_test, y_pred)
#print("Confusion Matrix:")
#print(cm)
print("-" * 30)

