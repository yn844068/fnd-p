import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score, classification_report
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier

# Load the dataset
#df = pd.read_csv('temp-p2-1k.csv')
df = pd.read_csv('temp-15k.csv')

columns_to_drop = [col for col in df.columns if col.startswith('lang')]
df = df.drop(columns=columns_to_drop)

df = df.drop(columns=['author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type', 'real_count', 'fake_count'])
print(list(df.columns))

X = df.drop(['id', 'flag_real'], axis=1)  # Exclude 'id' and 'flag_real' columns from features
y = df['flag_real']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Standardize the features
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

classifiers = [
    KNeighborsClassifier(n_neighbors=15, weights='uniform', p=1),
    LogisticRegression(C=0.01, solver='lbfgs'),
    DecisionTreeClassifier(max_depth=None, min_samples_leaf=1, min_samples_split=2, criterion='gini'),
    GradientBoostingClassifier(n_estimators=100, max_depth=3),
    RandomForestClassifier(max_depth=None, min_samples_leaf=1, min_samples_split=2, n_estimators=200)
]


for clf in classifiers:
    clf.fit(X_train_scaled, y_train)
    y_pred = clf.predict(X_test_scaled)

    
        # Calculate performance metrics
    conf_matrix = confusion_matrix(y_test, y_pred)
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)
    classification_rep = classification_report(y_test, y_pred)
    print("Classifier:", clf.__class__.__name__)
    print("Accuracy:", accuracy)
    print("Precision:", precision)
    print("Recall:", recall)
    print("F1 Score:", f1)
    print('Classification Report:\n', classification_rep)
    print("="*50)

