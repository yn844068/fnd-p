import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score,  confusion_matrix
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.svm import SVC
import warnings
import time
warnings.filterwarnings("ignore")

start_time = time.time()
df = pd.read_csv('temp-p2-1400000.csv')

cols_to_remove = [col for col in df.columns if col.startswith('lang')]

df = df.drop(columns=cols_to_remove)

#df = pd.read_csv('temp-p2-15000.csv')
df = df.drop(columns=['author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type'])
y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
cv_classifiers = {

    LogisticRegression(): {'C': [0.01, 0.1, 1, 10, 100],
                           'solver': ['lbfgs', 'liblinear', 'sag', 'saga']},

    KNeighborsClassifier(): {'n_neighbors': [5, 10, 15],
                             'weights': ['uniform', 'distance'],
                             'p': [1, 2]}

}



cv_classifiers = {
    DecisionTreeClassifier(): {'criterion': ['gini', 'entropy'],
                              'max_depth': [None, 10, 20, 30, 50],
                              'min_samples_split': [2, 5, 10],
                              'min_samples_leaf': [1, 2, 4]},


    LogisticRegression(): {'C': [0.01, 0.1, 1, 10, 100],
                           'solver': ['lbfgs', 'liblinear', 'sag', 'saga']},

    KNeighborsClassifier(): {'n_neighbors': [5, 10, 15],
                             'weights': ['uniform', 'distance'],
                             'p': [1, 2]},

    RandomForestClassifier(random_state=42): {'n_estimators': [100, 200, 300],
                                              'max_depth': [None, 10, 20, 30],
                                              'min_samples_split': [2, 5, 10],
                                              'min_samples_leaf': [1, 2, 4]}

}

print('=========   GridSearchCV  section   ============== ')
# Perform GridSearchCV for each classifier
for clf, param_grid in cv_classifiers.items():
    grid_search = GridSearchCV(clf, param_grid, cv=5, scoring='accuracy')
    grid_search.fit(X, y)

    print("Classifier: ", clf.__class__.__name__)
    print("Best parameters:", grid_search.best_params_)
    print("Best cross-validated accuracy:", grid_search.best_score_)

    results = grid_search.cv_results_
    for mean_score, params in zip(results['mean_test_score'], results['params']):
        print("Parameters:", params)
        print("Mean Test Score (Accuracy):", mean_score)
        print()

    print("=" * 40)

end_time = time.time()
elapsed_time = end_time - start_time
print("Elapsed time for load:"+ str(elapsed_time)+ "seconds\n")


