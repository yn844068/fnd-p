import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Flatten
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import pandas as pd
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from tensorflow import keras
from tensorflow.keras import layers


df = pd.read_csv('temp-p2-1400000.csv')
#df = pd.read_csv('temp-nn.csv')

df = df.drop(columns=['author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type'])
print(list(df.columns))


print('boom')

X = df.drop(['id', 'flag_real'], axis=1)  # Exclude 'id' and 'flag_real' columns from features
y = df['flag_real']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

model = keras.Sequential([
    layers.Dense(64, activation='relu', input_shape=(X_train.shape[1],)),
    layers.Dropout(0.5),  # Regularization
    layers.Dense(64, activation='relu'),
    layers.Dense(1, activation='sigmoid')
])
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
#model.fit(X_train_scaled, y_train, epochs=10, batch_size=32)

# Model Training
early_stopping = keras.callbacks.EarlyStopping(patience=3, restore_best_weights=True)
model.fit(X_train_scaled, y_train, epochs=50, batch_size=32, validation_split=0.1, callbacks=[early_stopping])


# Evaluation
loss, accuracy = model.evaluate(X_test_scaled, y_test)
print('Test Loss:', loss)
print('Test Accuracy:', accuracy)


predictions = model.predict(X_test_scaled)

# Confusion Matrix and Metrics
from sklearn.metrics import confusion_matrix, classification_report

threshold = 0.5
y_pred = (predictions > threshold).astype(int)
conf_matrix = confusion_matrix(y_test, y_pred)
print('Confusion Matrix:\n', conf_matrix)
print('Classification Report:\n', classification_report(y_test, y_pred))




