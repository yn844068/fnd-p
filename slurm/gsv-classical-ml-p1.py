import time
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import classification_report
import xgboost as xgb

start_time = time.time()

#df = pd.read_csv('twitter-exp-p1-150000.csv')
df = pd.read_csv('tweets-exp-p1-15000.csv')
print('Running script exp-classical-ml-p1.py  ')
#print(df.head())
print(df.columns)
print('Using input twitter-exp-p1-150000.csv ')

from datetime import datetime
#df['user_created_date'] = pd.to_datetime(df['user_created_date'])
df['user_created_date'] = df['user_created_date'].apply(lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp())
df = df.drop(columns=['text','author'])



y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

print("Training set size: ", X_train.shape[0])
print("Testing set size: ", X_test.shape[0])



clf = RandomForestClassifier(random_state=42)
param_grid = {
    'n_estimators': [100, 200, 300],
    'max_depth': [None, 5, 10],
    'min_samples_split': [2, 5, 10]
}

print('Running GridSearchCV for RandomForestClassifier')
grid_search = GridSearchCV(clf, param_grid, cv=5)
grid_search.fit(X, y)
cv_results = grid_search.cv_results_

for i in range(grid_search.n_splits_):
    print("Fold {}:".format(i+1))
    print("Mean test score: {}".format(cv_results['mean_test_score'][i]))
    print("Mean train score: {}".format(cv_results['mean_train_score'][i]))
    print("Params: {}".format(cv_results['params'][i]))
    print("---")

print("Best parameters:", grid_search.best_params_)
print("Best score:", grid_search.best_score_)




print("-" * 30)
param_grid = {
    'n_neighbors': [3, 5, 7],
    'weights': ['uniform', 'distance'],
    'algorithm': ['auto', 'ball_tree', 'kd_tree', 'brute']
}

clf = KNeighborsClassifier()

# Create GridSearchCV
grid_search = GridSearchCV(clf, param_grid, cv=5)
grid_search.fit(X_train, y_train)
cv_results = grid_search.cv_results_

for i in range(grid_search.n_splits_):
    print("Fold {}:".format(i+1))
    print("Mean test score:", cv_results['mean_test_score'][i])
    print("Mean train score:", cv_results['mean_train_score'][i])
    print("Params:", cv_results['params'][i])
    print("---")

print("Best parameters:", grid_search.best_params_)
print("Best score:", grid_search.best_score_)

# Make predictions on the test set using the best model
best_model = grid_search.best_estimator_
y_pred = best_model.predict(X_test)

# Evaluate the model
report = classification_report(y_test, y_pred)
print("Classification Report KNeighborsClassifier:\n", report)
print("-" * 30)

param_grid = {
    'n_estimators': [100, 200, 300],  # Number of boosting rounds (trees)
    'learning_rate': [0.01, 0.1, 0.2],  # Step size at each iteration
    'max_depth': [3, 5, 7],  # Maximum depth of each tree
    'min_child_weight': [1, 3, 5],  # Minimum sum of instance weight (Hessian) needed in a child
    'subsample': [0.8, 1.0],  # Fraction of samples used for fitting the trees
    'colsample_bytree': [0.8, 1.0],  # Fraction of features used for fitting the trees
}

model = xgb.XGBClassifier(
    objective='binary:logistic',  # Binary classification problem
    random_state=42
)


grid_search = GridSearchCV(model, param_grid, cv=5)
grid_search.fit(X_train, y_train)
cv_results = grid_search.cv_results_

for i in range(grid_search.n_splits_):
    print("Fold {}:".format(i+1))
    print("Mean test score:", cv_results['mean_test_score'][i])
    print("Mean train score:", cv_results['mean_train_score'][i])
    print("Params:", cv_results['params'][i])
    print("---")

print("Best parameters:", grid_search.best_params_)
print("Best score:", grid_search.best_score_)

# Make predictions on the test set using the best model
best_model = grid_search.best_estimator_
y_pred = best_model.predict(X_test)
report = classification_report(y_test, y_pred)
print("Classification Report XGBClassifier:\n", report)




elapsed_time = time.time() - start_time
print("Elapsed time:", elapsed_time, "seconds")

