#!/bin/bash

#SBATCH -p long
#SBATCH -o job-class-ml-p2-a.out
#SBATCH --time=3-00:00:00
#SBATCH --mem=32G

module load anaconda
source activate myenv
cd /home/users/yn844068/dissertation/slurm
python fafa-classical-ml-p2.py
