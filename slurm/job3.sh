#!/bin/bash

#SBATCH -p scavenger 
#SBATCH -o job-prepare-a.out
#SBATCH --time=4-00:00:00
#SBATCH --mem=64G

module load anaconda
source activate myenv
cd /home/users/yn844068/dissertation/slurm
python fafa-prepare-slurm.py
