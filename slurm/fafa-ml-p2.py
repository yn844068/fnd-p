import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Flatten
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler
import pandas as pd
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import confusion_matrix, classification_report

df = pd.read_csv('temp-p2-1400000.csv')
#df = pd.read_csv('temp-nn.csv')

df = df.drop(columns=['author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type'])
print(list(df.columns))


print('boom')

X = df.drop(['id', 'flag_real'], axis=1)  # Exclude 'id' and 'flag_real' columns from features
y = df['flag_real']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

gradient_boosting = GradientBoostingClassifier(random_state=42)

# Define parameter grid for Gradient Boosting in GridSearchCV
gb_param_grid = {
    'n_estimators': [100, 200, 300],
    'learning_rate': [0.01, 0.1, 0.2],
    'max_depth': [3, 4, 5],
    'min_samples_split': [2, 5, 10],
    'min_samples_leaf': [1, 2, 4]
}

# Create GridSearchCV object for Gradient Boosting
gb_grid_search = GridSearchCV(gradient_boosting, gb_param_grid, cv=5, scoring='accuracy', n_jobs=-1)

# Fit the GridSearchCV on the training data for Gradient Boosting
gb_grid_search.fit(X_train_scaled, y_train)

# Get the best estimator from the grid search
best_gb = gb_grid_search.best_estimator_

# Make predictions on the test data using the best estimator
y_pred_gb = best_gb.predict(X_test_scaled)

# Evaluate the Gradient Boosting model
conf_matrix_gb = confusion_matrix(y_test, y_pred_gb)
classification_rep_gb = classification_report(y_test, y_pred_gb)

print('Best Parameters for Gradient Boosting:', gb_grid_search.best_params_)
print('Confusion Matrix (Gradient Boosting):\n', conf_matrix_gb)
print('Classification Report (Gradient Boosting):\n', classification_rep_gb)




