#!/bin/bash

#SBATCH -p scavenger
#SBATCH -o job-class-ml-p2-b.out
#SBATCH --time=2-00:00:00
#SBATCH --mem=64G

module load anaconda
source activate myenv
cd /home/users/yn844068/dissertation/slurm
python fafa-classical-ml-p2.py
