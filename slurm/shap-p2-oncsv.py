import shap
import joblib
import pandas as pd
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import warnings
import numpy as np
warnings.filterwarnings("ignore")
import matplotlib
matplotlib.use('Agg') 
print('welcome to shap')


df = pd.read_csv('temp-p2-1400000.csv')
df = df.drop(columns=['id','author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type'])
print(list(df.columns))


y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

num_columns = X_test.shape[1]
print("Number of columns in X_test:", num_columns)

clf = RandomForestClassifier(n_estimators=100, random_state=42)

clf_name = clf.__class__.__name__
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print(f"Classifier: {clf_name}")
print(f"Accuracy: {accuracy:.4f}")
print("-" * 30)


background_summary = shap.sample(X_train, 100)
explainer = shap.KernelExplainer(clf.predict_proba, background_summary)
shap_values = explainer.shap_values(X_test)

shap.summary_plot(shap_values[1], X_test, feature_names=df.columns)
plt.savefig("shap_summary_plot.png")
plt.close() 
