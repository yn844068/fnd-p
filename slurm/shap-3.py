import shap
import joblib
import pandas as pd
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import warnings
import numpy as np
warnings.filterwarnings("ignore")
import matplotlib
matplotlib.use('Agg') 


print('welcome to shap')

df = pd.read_csv('temp-p2-1k.csv')
from datetime import datetime


df = df.drop(columns=['id','author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type'])
print(list(df.columns))
column_list = df.columns.tolist()
column_list.remove('flag_real')

y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

num_columns = X_test.shape[1]
print("Number of columns in X_test:", num_columns)

clf = RandomForestClassifier(n_estimators=100, random_state=42)

clf_name = clf.__class__.__name__
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print(f"Classifier: {clf_name}")
print(f"Accuracy: {accuracy:.4f}")
print("-" * 30)




background_summary = shap.sample(X_train, 50)
explainer = shap.KernelExplainer(clf.predict_proba, background_summary)
shap_values = explainer.shap_values(X_test)

shap.summary_plot(shap_values[1], X_test, feature_names=df.columns)
plt.savefig("shap_summary_plot_p2.png")
plt.close() 

print("***" * 40)
print()


print('-column_list----->', column_list)
# Create a list of pandas Series for each feature's SHAP values
shap_values_series = [pd.Series(sv, name=feature) for sv, feature in zip(shap_values[0], column_list)]

# Create the shap_df DataFrame by concatenating the list of Series
shap_df = pd.concat(shap_values_series, axis=1)

# Calculate feature importance by averaging absolute SHAP values
feature_importance = shap_df.abs().mean()

# Sort features by importance
feature_importance_ranking = feature_importance.sort_values(ascending=False)

# Print or visualize the feature importance ranking
print(feature_importance_ranking)


# Set a threshold for feature selection (e.g., keep top 5 features)
top_features = feature_importance_ranking.head(5).index

print(top_features)


