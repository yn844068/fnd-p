import shap
import joblib
import pandas as pd
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import warnings
import numpy as np
warnings.filterwarnings("ignore")
import matplotlib
matplotlib.use('Agg') 
from datetime import datetime


print('welcome to shap')

df = pd.read_csv('temp-p2-1k.csv')
print(list(df.columns))
columns_to_remove = [col for col in df.columns if col.startswith('lang')]

# Remove the specified columns from the DataFrame
df = df.drop(columns=columns_to_remove)
print(list(df.columns))

#df = df.drop(columns=['id','author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type'])



