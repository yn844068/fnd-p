import pandas as pd
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score,  confusion_matrix
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.svm import SVC
import warnings
import time
import joblib
from sklearn.preprocessing import OneHotEncoder
warnings.filterwarnings("ignore")

start_time = time.time()
#df = pd.read_csv('tweets-exp-p2-15000.csv')
df = pd.read_csv('tweets-exp-p2-1400000.csv')
#file.write(df.columns)
#print(df.head(5))
print('Loading file tweets-exp-p2-1400000.csv ')
selected_columns = ['author_id', 'posted_at', 'days_elapsed_created', 'days_elapsed_posted']

end_time = time.time()
elapsed_time = end_time - start_time
print("Elapsed time for load:"+ str(elapsed_time)+ "seconds\n")

df['user_created_date'] = pd.to_datetime(df['user_created_date'], format='%Y-%m-%dT%H:%M:%S.%fZ')
current_date = datetime.now()
df['days_elapsed_created'] = (current_date - df['user_created_date']).dt.days


df['posted_at'] = pd.to_datetime(df['posted_at'], format='%a %b %d %H:%M:%S +0000 %Y').dt.date
current_date = datetime.now().date()
df['days_elapsed_posted'] = (current_date - df['posted_at']).dt.days

#print(df.loc[:, selected_columns].head(20))
# Sort DataFrame chronologically based on 'posted_at'
df.sort_values(by='posted_at', ascending=True, inplace=True)
#print(df.loc[:, selected_columns].head(20))

print('sorted')

df['real_count'] = df[df['flag_real'] == True].groupby('author_id')['author_id'].transform('count')
df['fake_count'] = df[df['flag_real'] == False].groupby('author_id')['author_id'].transform('count')
df['real_count'].fillna(0, inplace=True)
df['fake_count'].fillna(0, inplace=True)
#df['user_created_date'] = df['user_created_date'].dt.strftime('%Y-%m-%d')
#df['posted_at'] = df['posted_at'].dt.strftime('%Y-%m-%d')

print('Found repeat counts')
df['tweetlanguage'] = df['tweetlanguage'].astype('category')
# Create one-hot encoded columns using pandas get_dummies
languages_df = pd.get_dummies(df['tweetlanguage'], prefix='lang')

# Concatenate the original DataFrame and the encoded features
df_encoded = pd.concat([df, languages_df], axis=1)
print('added onehotencoder for tweetlanguage')
print(df_encoded.columns)
df_encoded.to_csv('temp-p2-1400000.csv', index=False)
end_time = time.time()
elapsed_time = end_time - start_time
print("Elapsed time for preprocessing:"+ str(elapsed_time)+ "seconds\n")


