import pandas as pd
import time
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score,confusion_matrix
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV


from sklearn.model_selection import train_test_split
start_time = time.time()
df = pd.read_csv('tweets-exp-p1-1400000.csv')
#df = pd.read_csv('tweets-exp-p1-15000.csv')

#print(df.head(5))

print('Running script exp-classical-ml-p1.py  ')
print('Using input tweets-exp-p1-1400000.csv ')
print(list(df.columns))

df['user_created_date'] = df['user_created_date'].apply(lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp())
df = df.drop(columns=['text','author'])
#df = df.drop(columns=['retweet_count', 'reply_count', 'like_count', 'tweet_count',  'listed_count',  'quote_count', 'impression_count', 'followers_count', 'following_count'])
y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

print("Training set size: ", X_train.shape[0])
print("Testing set size: ", X_test.shape[0])


classifiers = [
    KNeighborsClassifier(n_neighbors=5,weights='uniform', algorithm='auto'),
    LogisticRegression(),
    RandomForestClassifier(n_estimators=200, max_depth=5,min_samples_split=2)
]
print("-" * 30)
for clf in classifiers:
    clf_name = clf.__class__.__name__
    
    # Fit the classifier on the training data
    clf.fit(X_train, y_train)
    
    # Make predictions on the test data
    y_pred = clf.predict(X_test)
    
    # Calculate accuracy
    accuracy = accuracy_score(y_test, y_pred)
    
    # Print accuracy and other performance details
    print("Classifier: ", clf_name)
    print("Accuracy: ",accuracy )
    
    # Print confusion matrix
    cm = confusion_matrix(y_test, y_pred)
    print("Confusion Matrix:")
    print(cm)
    print("-" * 30)
    print("-" * 30)



elapsed_time = time.time() - start_time
print("Elapsed time:", elapsed_time, "seconds")
