import csv
import time

small_file = "D:\\data-science\\project\\experiments\\twitter-data-p1-1k.csv"
large_file = "D:\\data-science\\project\\experiments\\twitter-data-p2-all.csv"
target_file = "D:\\data-science\\project\\experiments\\twitter-data-p2-1kb.csv"

count = 0
start_time = time.time()
try:
    with open(small_file, 'r', encoding='utf-8') as reader, open(target_file, 'w', encoding='utf-8') as writer:
        csv_parser = csv.reader(reader)
        for record in csv_parser:
            tweet_id = record[0]  # Assuming the tweet ID is in the first column
            print(f"{tweet_id} {count}")
            count += 1
            with open(large_file, 'r', encoding='utf-8') as p2_reader:
                for line in p2_reader:
                    if tweet_id in line:
                        writer.write(line + "\n")
except IOError as e:
    print("IOError:", e)
finally:
    end_time = time.time()
    execution_time = end_time - start_time
    print("Execution time:", execution_time, "seconds")
