import pandas as pd
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score,  confusion_matrix
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.svm import SVC
import warnings
import time
import joblib
from sklearn.preprocessing import OneHotEncoder
warnings.filterwarnings("ignore")

start_time = time.time()
df = pd.read_csv('tweets-exp-p2-15000.csv')
#file.write(df.columns)
#print(df.head(5))

end_time = time.time()
elapsed_time = end_time - start_time
print("Elapsed time for load:"+ str(elapsed_time)+ "seconds\n")

end_time = time.time()
elapsed_time = end_time - start_time
print("Elapsed time for load:"+ str(elapsed_time)+ "seconds\n")

print(list(df.columns))
df['user_created_date'] = pd.to_datetime(df['user_created_date'], format='%Y-%m-%dT%H:%M:%S.%fZ')
current_date = datetime.now()
df['days_elapsed_created'] = (current_date - df['user_created_date']).dt.days

df['posted_at'] = pd.to_datetime(df['posted_at'], format='%a %b %d %H:%M:%S %z %Y')
current_date = datetime.now(df['posted_at'].iloc[0].tzinfo)  # Use the timezone from the first 'posted_at' value
df['days_elapsed_posted'] = (current_date - df['posted_at']).dt.days

# Sort DataFrame chronologically based on 'posted_at'
df = df.sort_values(by='posted_at', ascending=False)

author_counts = {}
num_of_repeats = []
for index, row in df.iterrows():
    author_id = row['author_id']
    if author_id in author_counts:
        author_counts[author_id] += 1
    else:
        author_counts[author_id] = 1
    num_of_repeats.append(author_counts[author_id])
    #print('author_id=',author_id, '       counts=',author_counts[author_id])
            


#print('author_counts =',author_counts) 
repeats_series = pd.Series(num_of_repeats, name='num_of_repeats')
df = pd.concat([df, repeats_series], axis=1)

df.to_csv('temp.csv', index=False)
print(df.head(10))


df['tweetlanguage'] = df['tweetlanguage'].astype('category')
# Create one-hot encoded columns using pandas get_dummies
languages_df = pd.get_dummies(df['tweetlanguage'], prefix='lang')

# Concatenate the original DataFrame and the encoded features
df_encoded = pd.concat([df, languages_df], axis=1)

print(df_encoded.columns)
end_time = time.time()
elapsed_time = end_time - start_time
print("Elapsed time for preprocessing:"+ str(elapsed_time)+ "seconds\n")

