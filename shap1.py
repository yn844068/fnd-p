import shap
import joblib
import pandas as pd
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import warnings
warnings.filterwarnings("ignore")

print('welcome to shap')

df = pd.read_csv('tweets-exp-p1-1500.csv')
from datetime import datetime
#df['user_created_date'] = pd.to_datetime(df['user_created_date'])
df['user_created_date'] = df['user_created_date'].apply(lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp())
df = df.drop(columns=['text', 'author'])



y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
print("Training set size: ", X_train.shape[0])
print("Testing set size: ", X_test.shape[0])


clf = RandomForestClassifier(n_estimators=100, random_state=42)

clf_name = clf.__class__.__name__
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print(f"Classifier: {clf_name}")
print(f"Accuracy: {accuracy:.4f}")
print("-" * 30)


explainer = shap.TreeExplainer(clf)
#shap_values = explainer.shap_values(X)

shap_values = explainer.shap_values(X_test)
shap.summary_plot(shap_values[1], X_test, plot_type="bar")


#explainer = shap.Explainer(clf)

# Calculate SHAP values for the "verified" feature
#shap_values = explainer.shap_values(df["verified"])

# Create a dependence plot
#shap.dependence_plot("verified", shap_values, df["verified"])
plt.savefig('dependence_plot.png')
