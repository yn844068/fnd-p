import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score,  confusion_matrix
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.svm import SVC
import warnings
warnings.filterwarnings("ignore")


df = pd.read_csv('tweets-exp-p2-15000.csv')
#print(df.columns)
#print(df.head(5))


print(list(df.columns))
df['user_created_date'] = df['user_created_date'].apply(lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp())
df['posted_at'] = df['posted_at'].apply(lambda x: datetime.strptime(x, '%a %b %d %H:%M:%S %z %Y').timestamp())

df['posted_at'] = pd.to_datetime(df['posted_at']).dt.tz_localize('UTC')
current_time_utc = pd.Timestamp.now(tz='UTC')
df['no_days_posted'] = (current_time_utc - df['posted_at']).dt.days


df['user_created_date'] = pd.to_datetime(df['user_created_date']).dt.tz_localize('UTC')
current_time_utc = pd.Timestamp.now(tz='UTC')
df['no_days_user_created'] = (current_time_utc - df['user_created_date']).dt.days


df = df.drop(columns=['author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type'])
print(list(df.columns))


#df = df.drop(columns=['retweet_count', 'reply_count', 'like_count', 'tweet_count',  'listed_count',  'quote_count', 'impression_count', 'followers_count', 'following_count'])
y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

#print("Training set size: ", X_train.shape[0])
#print("Testing set size: ", X_test.shape[0])

classifiers = [
    KNeighborsClassifier(n_neighbors=5),
    RandomForestClassifier(n_estimators=100, random_state=42)
]

for clf in classifiers:
    clf_name = clf.__class__.__name__
    
    # Fit the classifier on the training data
    clf.fit(X_train, y_train)
    
    # Make predictions on the test data
    y_pred = clf.predict(X_test)
    
    # Calculate accuracy
    accuracy = accuracy_score(y_test, y_pred)
    
    # Print accuracy and other performance details
    print(f"Classifier: {clf_name}")
    print(f"Accuracy: {accuracy:.4f}")
    
    # Print confusion matrix
    cm = confusion_matrix(y_test, y_pred)
    #print("Confusion Matrix:")
    #print(cm)
    
    print("-" * 30)
