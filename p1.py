import pandas as pd
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC


df = pd.read_csv('twitter-onel.csv')
#df['user_created_date'] = pd.to_datetime(df['user_created_date'])
df['user_created_date'] = df['user_created_date'].apply(lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp())
df = df.drop(columns=['text', 'author'])



y = df["flag_real"].values
X = df.drop("flag_real", axis=1).values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

print("Training set size: ", X_train.shape[0])
print("Testing set size: ", X_test.shape[0])


# Create a Decision Tree Classifier instance
#clf = DecisionTreeClassifier()
#clf = GaussianNB()
#clf = LogisticRegression()
#clf = KNeighborsClassifier(n_neighbors=5)
#clf = RandomForestClassifier(n_estimators=100, random_state=42)
clf= SVC(kernel='rbf', C=1.0, gamma='scale')

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Fit the classifier to the training data
clf.fit(X_train, y_train)

# Make predictions on the testing data
predictions = clf.predict(X_test)

# Evaluate the accuracy of the classifier
accuracy = accuracy_score(y_test, predictions)
print("Accuracy:", accuracy)







