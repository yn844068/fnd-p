import pandas as pd

# Assuming your CSV file is named "follower_data.csv" and has columns "followerid" and "targetid"
# Read the CSV file into a pandas DataFrame
df = pd.read_csv("combined_network_with_header.csv", header=None, names=["followerid", "targetid"])

follower_ids_with_followers = df["targetid"].unique()
zero_followers_ids = df[~df["followerid"].isin(follower_ids_with_followers)]

# Step 3: Remove the rows corresponding to these user IDs from the DataFrame
df = df[~df["followerid"].isin(zero_followers_ids["followerid"])]

# Step 4: Save the updated DataFrame back to a new CSV file
df.to_csv("follower_data_updated.csv", index=False)