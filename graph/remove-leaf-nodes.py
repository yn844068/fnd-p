import networkx as nx
import pandas as pd

df = pd.read_csv('follower_data_updated.csv')

G = nx.DiGraph()

for _, row in df.iterrows():
    follower = row[0]
    target = row[1]
    G.add_edge(follower, target)

# Find leaf nodes (degree 1) and remove them
leaf_nodes = [node for node in G.nodes() if G.degree(node) == 1]
G.remove_nodes_from(leaf_nodes)

# Print the updated graph
print(G.nodes())
print(G.edges())


# Save the graph to a file
nx.write_gexf(G, "graph.gexf")

# Later, restore the graph from the file
#restored_G = nx.read_gexf("graph.gexf")
#print(restored_G.edges())

reduced_edges = G.edges()
reduced_data = [{'followerid': source, 'targetid': target} for source, target in reduced_edges]
reduced_df = pd.DataFrame(reduced_data)

print(reduced_df)
reduced_df.to_csv('graph-wo-leaf.csv', index=False)

