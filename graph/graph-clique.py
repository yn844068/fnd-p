import networkx as nx
import pandas as pd
import matplotlib
import numpy as np
import csv
import matplotlib
matplotlib.use('Agg')
import itertools
import matplotlib.pyplot as plt

# Read the CSV file into a pandas DataFrame
df = pd.read_csv('combined_twitter_network1.csv')

# Create an empty DiGraph
G = nx.DiGraph()

# Add edges to the graph
for _, row in df.iterrows():
    follower = row[0]
    target = row[1]
    G.add_edge(follower, target)

'''
print('Number of edges: ', G.number_of_edges() )
print('Number of nodes: ', G.number_of_nodes() )

'''
undirected_G = G.to_undirected()

strongly_connected_components  = list(nx.connected_components(undirected_G))
sorted_components = sorted(strongly_connected_components, key=len, reverse=True)

print('a')
# Get the largest strongly connected component (first component in sorted list)
largest_component = sorted_components[0]

# Remove nodes from the largest strongly connected component
G.remove_nodes_from(largest_component)



undirected_G = G.to_undirected()

'''
# Find cliques in the directed graph
cliques = list(nx.find_cliques(undirected_G))
# Print the cliques
for idx, clique in enumerate(cliques, start=1):
    print(f'Clique {idx}: {clique}')
'''
print('c')



strongly_connected_components  = list(nx.connected_components(undirected_G))
sorted_components = sorted(strongly_connected_components, key=len, reverse=True)


'''
# Filter and print strongly connected components with more than 2 nodes
for component in sorted_components:
    component_size = len(component)
    print(component_size)
    if 70 < component_size < 7500:
        print("Strongly Connected Component size:", len(component))
        #print("Component elements are: \n",component)
'''


df2 = pd.read_csv('author_tweet_counts-all-fakedegree.csv')
# Create a dictionary to map author_id to fake_degree
fake_degree_mapping = dict(zip(df2['author_id'], df2['fake_degree']))

print('d')

node_colors = [fake_degree_mapping.get(node, 'grey') for node in G.nodes()]
plt.figure(figsize=(20, 10))
plt.axis("off")
#pos = nx.spring_layout(G, iterations=25)
#nx.draw_networkx(G, pos=pos, node_size=20, with_labels=False, node_color=node_colors, width=0.15)

pos = nx.spiral_layout(G)  # Use spiral layout
nx.draw_networkx(
    G,
    pos=pos,
    with_labels=False,
    node_color=node_colors,
    width=0.1,  # Adjust edge width
    alpha=0.8,  # Adjust transparency
)





# Save the graph to a file
plt.savefig("clique_colored_graph.png", format="png")


'''
# Visualize the original graph
pos = nx.spring_layout(G, seed=42)
nx.draw(G, pos, with_labels=True, node_size=500, font_size=10, font_color='black', node_color='lightblue')
plt.title("Original Social Network")
#plt.show()
plt.savefig('a-degree_centrality.jpg', format='jpeg')

# Perform community detection using Louvain method
communities = nx.algorithms.community.greedy_modularity_communities(G)

# Create a new graph with community information
communities_graph = nx.Graph()
for idx, community in enumerate(communities):
    for node in community:
        communities_graph.add_node(node, community=idx)
    for pair in itertools.combinations(community, 2):
        communities_graph.add_edge(*pair)

# Visualize the graph with communities
node_colors = [communities_graph.nodes[node]['community'] for node in communities_graph.nodes]
nx.draw(communities_graph, pos, node_color=node_colors, with_labels=False, node_size=500, font_size=10, font_color='black', cmap=plt.cm.Set1)
plt.title("Community Detection Result")
#plt.show()
plt.savefig('b-degree_centrality.jpg', format='jpeg')

'''