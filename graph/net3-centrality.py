import networkx as nx
import pandas as pd
import matplotlib
import numpy as np
import csv
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt

# Read the CSV file into a pandas DataFrame
df = pd.read_csv('combined_twitter_network1.csv')

# Create an empty DiGraph
G = nx.DiGraph()

# Add edges to the graph
for _, row in df.iterrows():
    follower = row[0]
    target = row[1]
    G.add_edge(follower, target)


print('Number of edges: ', G.number_of_edges() )
print('Number of nodes: ', G.number_of_nodes() )

avg_degree = np.mean([d for _, d in G.degree()])
print(' On average, a node is connected to ',avg_degree)



degree_centrality = nx.centrality.degree_centrality(G)
keys = list(degree_centrality.keys())
values = list(degree_centrality.values())
# Sort the values in descending order
values, keys = zip(*sorted(zip(values, keys), reverse=True))
# Select the top 10 values
top_values = values[:10]
top_keys = keys[:10]
x_pos = range(len(top_keys))
plt.bar(x_pos, top_values, align='center')
plt.xticks(x_pos, top_keys)
plt.xticks(rotation=45)
plt.xlabel('Nodes')
plt.ylabel('Degree')
plt.title('Top 10 nodes with highest degree')
plt.savefig('degree_centrality.jpg', format='jpeg')
#print('keys :', keys)
author_ids_to_remove = keys

combined_data = list(zip(keys, values))
# Save the combined data to a CSV file
with open('degree_centrality_data.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    # Write the header row
    writer.writerow(['Keys', 'Values'])
    # Write the data rows
    writer.writerows(combined_data)



betweenness_centrality = nx.centrality.betweenness_centrality(G)
keys = list(betweenness_centrality.keys())
values = list(betweenness_centrality.values())
# Sort the values in descending order
values, keys = zip(*sorted(zip(values, keys), reverse=True))
top_values = values[:10]
top_keys = keys[:10]
x_pos = range(len(top_keys))

plt.bar(x_pos, top_values, align='center')

plt.xticks(x_pos, top_keys)
plt.xticks(rotation=45)
plt.xlabel('Nodes')
plt.ylabel('Betweenness Centrality')
plt.title('Top 10 nodes with highest betweenness centrality')
plt.savefig('betweenness centrality.jpg', format='jpeg')

combined_data_b = list(zip(keys, values))
# Save the combined data to a CSV file
with open('betweenness_centrality_data.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    # Write the header row
    writer.writerow(['Keys', 'Values'])
    # Write the data rows
    writer.writerows(combined_data_b)

