import networkx as nx
import matplotlib.pyplot as plt
G = nx.DiGraph()

nodes = [1, 2, 3, 4]
G.add_nodes_from(nodes)

edges = [(1, 2, {'color': 'red'}), (2, 3, {'color': 'blue'}), (3, 4, {'color': 'green'})]
G.add_edges_from(edges)

pos = nx.spring_layout(G)  # You can use different layout algorithms
edge_colors = [edge[2]['color'] for edge in G.edges(data=True)]

nx.draw(G, pos, with_labels=True, node_color='lightgray', edge_color=edge_colors, arrows=True)
edge_labels = {(edge[0], edge[1]): edge[2]['color'] for edge in G.edges(data=True)}
nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels)

plt.show()
