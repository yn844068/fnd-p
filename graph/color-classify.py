import pandas as pd

df = pd.read_csv('author_tweet_counts-all.csv')


# Calculate fake_degree based on conditions
def classify_fake_degree(row):
    total_tweet_count = row['fake_tweet_count'] + row['real_tweet_count']
    threshold = total_tweet_count * 0.2
    
    if abs(row['fake_tweet_count'] - row['real_tweet_count']) < threshold:
        return 'blue'
    elif row['fake_tweet_count'] > row['real_tweet_count']:
        return 'red'
    else:
        return 'green'

df['fake_degree'] = df.apply(classify_fake_degree, axis=1)

df.to_csv('author_tweet_counts-all-fakedegree.csv', index=False)
