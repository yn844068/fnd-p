import networkx as nx
import pandas as pd
import matplotlib
import numpy as np
import csv
import matplotlib
matplotlib.use('Agg')
import itertools
import matplotlib.pyplot as plt

# Read the CSV file into a pandas DataFrame
df = pd.read_csv('follower_data_updated.csv')

# Create an empty DiGraph
G = nx.DiGraph()

# Add edges to the graph
for _, row in df.iterrows():
    follower = row[0]
    target = row[1]
    G.add_edge(follower, target)

print('Number of edges: ', G.number_of_edges() )
print('Number of nodes: ', G.number_of_nodes() )

# Calculate centrality measures
closeness_centrality = nx.closeness_centrality(G)
degree_centrality = nx.degree_centrality(G)
betweenness_centrality = nx.betweenness_centrality(G)

# Add centrality measures as new features to the DataFrame
df['Degree_Centrality'] = df['followerid'].map(degree_centrality)
df['Betweenness_Centrality'] = df['followerid'].map(betweenness_centrality)
df['Closeness_Centrality'] = df['followerid'].map(closeness_centrality)  # Assuming 'followerid' is the column with node IDs

# Save the DataFrame with the new feature to a new CSV file
df.to_csv('combined_twitter_network_with_centrality.csv', index=False)

print("centrality values saved to CSV.")


# Define the layout using Kamada-Kaway layout algorithm
layout = nx.spring_layout(G)

# Draw the graph with nodes and edges
nx.draw(G, pos=layout, with_labels=False, node_size=500)

# Add a title
plt.title("Visually Appealing Graph with Kamada-Kaway Layout")
plt.savefig('a-Kamada-Kaway.jpg', format='jpeg')
# Show the plot






