import matplotlib
matplotlib.use('Agg')

import networkx as nx
import matplotlib.pyplot as plt
import random
import pandas as pd

# Create a DiGraph
G = nx.DiGraph()

#df1 = pd.read_csv('graph-trimmed.csv')
df1 = pd.read_csv('follower_data_updated.csv')


df2 = pd.read_csv('author_tweet_counts-all-fakedegree.csv')


# Create a dictionary to map author_id to fake_degree
fake_degree_mapping = dict(zip(df2['author_id'], df2['fake_degree']))


# Create an empty DiGraph
G = nx.DiGraph()

# Add nodes and edges to the graph
for _, row in df1.iterrows():
    follower = row['followerid']
    target = row['targetid']
    
    G.add_edge(follower, target)

node_colors = [fake_degree_mapping.get(node, 'grey') for node in G.nodes()]

# Draw the graph with colored nodes
#pos = nx.spring_layout(G)
#nx.draw(G, pos, with_labels=True, node_color=node_colors, arrows=True)

#pos = nx.circular_layout(G)
#nx.draw(G, pos, node_color=node_colors, arrows=True)

plt.figure(figsize=(20, 10))
plt.axis("off")
pos = nx.spring_layout(G, iterations=25)
nx.draw_networkx(G, pos=pos, node_size=20, with_labels=False, node_color=node_colors, width=0.15)

# Save the graph to a file
plt.savefig("colored_graph.png", format="png")

