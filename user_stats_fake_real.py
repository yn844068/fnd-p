import pandas as pd
import numpy as np
from datetime import datetime

df = pd.read_csv('tweets-p2-all.csv')
#print(df.columns)
#print(df.head(5))


print(list(df.columns))


# Group the DataFrame by 'author_id' and count the occurrences of flag_real values
tweet_counts = df.groupby(['author_id', 'flag_real']).size().unstack(fill_value=0)


# Rename the columns and reset the index
tweet_counts.columns = ['fake_tweet_count', 'real_tweet_count']
tweet_counts.reset_index(inplace=True)

# Sort the DataFrame in descending order by 'fake_tweet_count' and then by 'real_tweet_count'
tweet_counts = tweet_counts.sort_values(by=['fake_tweet_count', 'real_tweet_count'], ascending=False)


# Save the result to a new CSV file
tweet_counts.to_csv('author_tweet_counts-all.csv', index=False)