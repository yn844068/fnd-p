from datetime import datetime

specific_date = datetime.strptime('2007-03-29', '%Y-%m-%d')
current_date = datetime.now()

days_elapsed = (current_date - specific_date).days

print(days_elapsed)