import pandas as pd
from wordcloud import WordCloud
import matplotlib.pyplot as plt


df = pd.read_csv('old\\twitter-exp-p1-150000.csv')
# Assuming you have a data frame called 'df' with a column named 'text'


word_cloud_text =  ' '.join(df['text'].astype(str))
#Creation of wordcloud
wordcloud = WordCloud(
    max_font_size=100,
    max_words=100,
    background_color="black",
    scale=10,
    width=800,
    height=800
).generate(word_cloud_text)


# Save the word cloud as an image file
wordcloud.to_file('wordcloud.png')
