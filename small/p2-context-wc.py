import pandas as pd
import regex as re

# Read the CSV files into DataFrames
df1 = pd.read_csv('tweets-p1-all.csv')
df2 = pd.read_csv('tweets-exp-p2-nw-filtered-small.csv')

# Define a function to calculate word count
def calculate_word_count(tweet_id):
    tweet = df1[df1["tweet_id"] == tweet_id]["text"].values[0]  # Use the correct column name
    return len(tweet.split())


# Function to calculate the average word length in a text
def calculate_average_word_length(tweet_id):
    tweet_text = df1[df1["tweet_id"] == tweet_id]["text"].values[0]  # Get tweet text from df1
    words = tweet_text.split()
    total_word_length = sum(len(word) for word in words)
    return total_word_length / len(words) if len(words) > 0 else 0  # Avoid division by zero

# Function to calculate emoji count in a text
def calculate_emoji_count(tweet_id):
    tweet_text = df1[df1["tweet_id"] == tweet_id]["text"].values[0]  # Get tweet text from df1
    emoji_count = sum(1 for char in tweet_text if char in "😀😃😄😁😆😅🤣😂🙂🙃😉😊😇🥰😍🤩😘😗😚😙😋😛😜🤪😝🤑🤗🤭🤫🤔🤐🤨😐😑😶😏😒🙄😬😮🤥😌😔😪🤤😴😷🤒🤕🤢🤮🤧🥵🥶🥴😵🤯🤠🥳😎🤓🧐😕😟😒😞😔😣😖😫😩😢😭😤😠😡🤬🤯😳😱😨😰😥😓🤗🙃🙂🧐🤔🤨😐😑😬🙁☹️😖😣😫😩😢😭😤😠😡🤬🤯😳😱😨😰😥😓🤗🙃🙂🧐🤔🤨😐😑😬🙁☹️😖😣😫😩😢😭😤😠😡🤬")
    return emoji_count

# Apply the function to create the 'average_word_length' column in df2
df2["average_word_length"] = df2["tweet_id"].apply(calculate_average_word_length)

# Apply the function to create the 'emoji_count' column in df2
df2["emoji_count"] = df2["tweet_id"].apply(calculate_emoji_count)

# Apply the function to create the 'word_count' column in df2
df2["word_count"] = df2["tweet_id"].apply(calculate_word_count)

df2.to_csv('tweets-exp-p2-nw-filtered-wordcount.csv', index=False) 
