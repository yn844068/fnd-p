import pandas as pd
import numpy as np
from datetime import datetime

df = pd.read_csv('tweets-exp-p2-nw-filtered-small.csv')
#print(df.columns)
#print(df.head(5))


print(list(df.columns))

df['real_count'] = df[df['flag_real'] == True].groupby('author_id')['author_id'].transform('count')
df['fake_count'] = df[df['flag_real'] == False].groupby('author_id')['author_id'].transform('count')
df['real_count'].fillna(0, inplace=True)
df['fake_count'].fillna(0, inplace=True)


# Save the result to a new CSV file
df.to_csv('tweets-small-with-counts.csv', index=False)