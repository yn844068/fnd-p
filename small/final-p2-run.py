import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score, classification_report

# Load the dataset
df = pd.read_csv('temp-p2-1k.csv')
# df = pd.read_csv('temp-nn.csv')

columns_to_drop = [col for col in df.columns if col.startswith('lang')]
df = df.drop(columns=columns_to_drop)

df = df.drop(columns=['author_id', 'user_created_date', 'posted_at', 'tweetlanguage', 'user_type'])
print(list(df.columns))

X = df.drop(['id', 'flag_real'], axis=1)  # Exclude 'id' and 'flag_real' columns from features
y = df['flag_real']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

classifiers = [
    KNeighborsClassifier(n_neighbors=5),
    RandomForestClassifier(n_estimators=100, random_state=42)
]

# Iterate over classifiers and evaluate their performance
for clf in classifiers:
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    
    # Calculate performance metrics
    conf_matrix = confusion_matrix(y_test, y_pred)
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)
    classification_rep = classification_report(y_test, y_pred)
    
    print("Classifier:", clf.__class__.__name__)
    print("Confusion Matrix:\n", conf_matrix)
    print("Accuracy:", accuracy)
    print("Precision:", precision)
    print("Recall:", recall)
    print("F1 Score:", f1)
    print("Classification Report:\n", classification_rep)
    print("="*50)
